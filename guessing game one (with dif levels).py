from random import randint

def numbers_game(a, b):
    a=randint(a,b)
    x=''
    while x!=a:
        x=int(input(''))
        if x<a:
            print('Za mała liczba!')
        elif x>a:
            print('Za duża liczba!')
        else:
            print('Gratulacje, trafiłeś dobrą liczbę!')

if __name__=='__main__':
    while True:
        print('Wybierz poziom trudności')
        print('1. Can I play, Daddy?')
        print('2. Don\'t hurt me.')
        print('3. Bring \'em on!')
        print('4. I am Death incarnate')
        print('5. Custom')
        print('0. Wyjście')
        try:
            choice=int(input('Twój wybór:'))
            if choice==0:
                break
            elif choice==1:
                numbers_game(1,10)
            elif choice==2:
                numbers_game(1,100)
            elif choice==3:
                numbers_game(1,1000)
            elif choice==4:
                numbers_game(1,10000)
            elif choice==5:
                a=int(input('Podaj dolny przedział:'))
                b=int(input('Podaj górny przedział:'))
                numbers_game(a,b)
            else:
                continue
        except:
            print('Źle podana liczba!')