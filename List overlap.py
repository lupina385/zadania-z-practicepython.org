import random

def overlap(a,b):
    return list((set(a)&set(b)))

if __name__=='__main__':
    a = [random.randint(0,100) for i in range(random.randint(0,30))]
    b = [random.randint(0,100) for i in range(random.randint(0,30))]
    print(a, b)
    print(overlap(a,b))